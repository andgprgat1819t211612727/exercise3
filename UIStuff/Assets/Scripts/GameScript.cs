﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScript : MonoBehaviour
{
    public State CurrentState;
    public Text uiText;
    // Start is called before the first frame update
    void Start()
    {
        UpdateDisplay();
    }
    
    void UpdateDisplay()
    {
        uiText.text = CurrentState.GetStateStoryText();
    }

    public void OnChoiceAClick()
    {
        CurrentState = CurrentState.GetNextState()[0];
        UpdateDisplay();
    }
    public void OnChoiceBClick()
    {
        CurrentState = CurrentState.GetNextState()[1];
        UpdateDisplay();
    }
}
