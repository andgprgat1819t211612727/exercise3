﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="State")]
public class State : ScriptableObject
{
    [TextArea(5, 10)] [SerializeField] string storyText;
    [SerializeField] State[] nextStates;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string GetStateStoryText()
    {
        return storyText;
    }

    public State[] GetNextState()
    {
        return nextStates;
    }
}
